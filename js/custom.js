// verifica se o input text está preenchido 
// Se estiver mantem estilo do focus
$(document).on('change', '.field-input-text', function() {
  if($(this).val() != '') {
    $(this).addClass('active');
  } else {
    $(this).removeClass('active');
  }
});


// verifica se o input checkbox de marcas está preenchido 
// Se estiver mantem estilo do hover
$(document).ready(function() {
  $('.field-label-brand').click(function() {
    var input = $(this).children('input');
    if (input.is(":checked")) {
      $(this).addClass('active');
    } else {
      $(this).removeClass('active');
    }
  });
});

// Abre e fecha filtros
$(document).ready(function() {
  $('.toggle-filter').click(function() {
    $('.box-filters').toggleClass('close-filter');
    $('.box-products').toggleClass('close-filter');
  });
});
